'''Kicks off processing input labels
and outputs to hardcoded directory

Notes: for larger scale usage I'd recommend using python click
to implement a simple CLI to handle neatly passing in command line parameters.
It also supports a progress bar via tdqm
'''
import logging
import os
from src.file_io import get_files, load_file_data, write_file_data
from src.filter import merge_overlaps

def runner():
    read_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'input_labels/')

    file_list = get_files(read_directory)

    # This bit can be parallelized if need be
    # Load files into memory from disk in batch
    # and perform the filtering in separate processes
    result_dict = {}
    for file in file_list:
        # write try except to catch possible parse and disk io errors
        try:
            data = load_file_data(os.path.join(read_directory, file))
            result_dict[file] = merge_overlaps(data)

        except Exception as e:
            logging.error("Error processing file: " + str(file) + ", " + str(e))

    # write results to disk (can be optimised as batch job, disk io tends to be serial in nature)
    write_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'filtered_data/')

    try:
        if not os.path.exists(write_directory):
            os.makedirs(write_directory)
    except Exception as e:
        logging.error(str(e))

    for file in file_list:
        try:
            write_file_data(os.path.join(write_directory, file), result_dict[file])

        except Exception as e:
            logging.error("Error processing file: " + str(file) + ", " + str(e))

if __name__ == "__main__":
    runner()
