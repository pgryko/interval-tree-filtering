# Interval tree for merging overlapping annotated events

Given an overlapped audio file with

Spec: takes a textfile with input
```
0.0 2.1 baby_cry
0.2 8.4 aircon_noise
2.0 3.4 dog_bark
3.2 4.2 dog_bark
3.8 6.0 baby_cry
```
and correctly merges overlaping labels and outputs them as text file e.g.
```
0.0 2.1 baby_cry
0.2 8.4 aircon_noise
2.0 4.2 dog_bark
3.8 6.0 baby_cry
```
## Algorithm

Use an interval tree - for handling overlaps.

Interval trees have a query time of O(log n+m) and an initial creation time of O(n log n)}, while limiting memory consumption to O(n). After creation, interval trees may be dynamic, allowing efficient insertion and deletion of an interval in O(log n) time.

Merging of overlaps can be done in O(n log n)
https://github.com/chaimleib/intervaltree/blob/master/intervaltree/intervaltree.py

Note: in some files the times are not ordered correctly e.g.
```
11.56 11.76 baby_cry
12.31 12.86 baby_cry
13.98 16.08 baby_cry
16.89 17.84 baby_cry
12.31 12.86 baby_cry
21.66 22.26 baby_cry
```
If times were ordered correctly, it might be possible to write an O(n) algorithm, where we are doing a linear scan and checking next vs previous boundary.

## Useful Commands

## Running example code to output results in filtered_data

```bash
$ python3 -m venv venv; source venv/bin/activate
$ pip install -r requirements.txt
$ python main.py
```

### Running tests
```bash
$ python -m pytest src/tests 
```

### Linting
Auto Lint using https://github.com/psf/black
```bash
$ black src
```

### Code quality
And check code quality using
```bash
$ pylint src
```

## Testing gitlab pipeline on localmachine

It's possible to install gitlab runner on your local machine and test the .gitlab-ci.yaml

```bash
$ gitlab-runner exec docker python_latest 
```

Note: This is setup on Gitlab with CI testing for python versions 3.6, 3.8 and python latest.
This way we have a hard check of compatibility between versions - and a fast + neat way to view tests + code coverage.