from typing import List, Tuple

from intervaltree import IntervalTree


def merge_overlaps(events_data: List[Tuple]):
    """Given a list of tuples of format (start,end, label)
    merge any overlaping regions for a label

    return a list of list[tuple], ordered by start

    :return list[tuple]
    """

    # We need 2 interval trees as the intervaltree module
    # does not support merge overlaps by label
    # If a more efficient algorithm is required
    # I'd look into forking the github repo and implementing a merge by label

    total_interval_tree = IntervalTree()

    partion_by_event_type = {}

    for event in events_data:
        if event[2] not in partion_by_event_type:
            partion_by_event_type[event[2]] = [event]
        else:
            partion_by_event_type[event[2]].append(event)

    for event_type, v in partion_by_event_type.items():

        event_type_interval_tree = IntervalTree()
        for event in v:
            event_type_interval_tree.addi(*event)

        # merge completes in O(n * log n)
        # the data_reducer maintains label consistency for same type
        event_type_interval_tree.merge_overlaps(
            strict=True, data_reducer=lambda prev, _: prev
        )
        total_interval_tree = total_interval_tree | event_type_interval_tree

    # There may be a more efficient way of extracting the ordered data,
    # to avoid the additional sort
    return [(x[0], x[1], x[2]) for x in sorted(total_interval_tree)]
