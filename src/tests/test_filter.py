import unittest
import os

from src.filter import merge_overlaps


class TestMergeOverlaps(unittest.TestCase):
    def setUp(self) -> None:
        self.input_data = [
            [
                (0.0, 2.1, "baby_cry"),
                (0.2, 8.4, "aircon_noise"),
                (2.0, 3.4, "dog_bark"),
                (3.2, 4.2, "dog_bark"),
                (3.8, 6.0, "baby_cry"),
            ],
            [
                (1.45, 2.06, "doorbell"),
                (3.51, 3.63, "dog_bark"),
                (4.23, 4.35, "dog_bark"),
                (4.99, 5.15, "dog_bark"),
                (6.49, 6.61, "dog_bark"),
                (7.7, 7.81, "dog_bark"),
                (11.4, 12.11, "dog_bark"),
                (11.59, 12.8, "dog_bark"),
                (16.3, 16.43, "dog_bark"),
                (26.86, 26.98, "dog_bark"),
            ],
            [
                (5.45, 5.57, "baby_cry"),
                (5.74, 6.14, "baby_cry"),
                (6.7, 7.6, "baby_cry"),
                (7.53, 8.93, "baby_cry"),
                (8.67, 9.01, "baby_cry"),
                (17.84, 18.02, "baby_cry"),
                (18.85, 19.33, "baby_cry"),
                (19.89, 20.45, "baby_cry"),
            ],
            [
                (11.56, 11.76, "baby_cry"),
                (12.31, 12.86, "baby_cry"),
                (13.98, 16.08, "baby_cry"),
                (16.89, 17.84, "baby_cry"),
                (12.31, 12.86, "baby_cry"),
                (21.66, 22.26, "baby_cry"),
            ],
            [
                (2.34, 3.55, "knocking"),
                (4.02, 4.13, "dog_bark"),
                (4.88, 5.01, "dog_bark"),
                (10.45, 12.02, "knocking"),
                (11.34, 11.46, "dog_bark"),
                (12.13, 12.29, "dog_bark"),
                (18.09, 18.17, "dog_bark"),
            ],
            [
                (0.0, 2.1, "baby_cry"),
                (0.2, 3.6, "aircon_noise"),
                (2.0, 3.4, "dog_bark"),
                (3.0, 4.0, "aircon_noise"),
                (3.2, 4.2, "dog_bark"),
                (3.5, 8.4, "aircon_noise"),
                (3.8, 6.0, "baby_cry"),
            ],
            [
                (2.11, 2.64, "baby_cry"),
                (3.92, 4.14, "baby_cry"),
                (5.62, 5.74, "baby_cry"),
                (6.31, 6.5, "baby_cry"),
                (6.5, 6.52, "baby_cry"),
                (7.5, 7.72, "baby_cry"),
            ],
        ]

        self.expected = [
            [
                (0.0, 2.1, "baby_cry"),
                (0.2, 8.4, "aircon_noise"),
                (2.0, 4.2, "dog_bark"),
                (3.8, 6.0, "baby_cry"),
            ],
            [
                (1.45, 2.06, "doorbell"),
                (3.51, 3.63, "dog_bark"),
                (4.23, 4.35, "dog_bark"),
                (4.99, 5.15, "dog_bark"),
                (6.49, 6.61, "dog_bark"),
                (7.7, 7.81, "dog_bark"),
                (11.4, 12.8, "dog_bark"),
                (16.3, 16.43, "dog_bark"),
                (26.86, 26.98, "dog_bark"),
            ],
            [
                (5.45, 5.57, "baby_cry"),
                (5.74, 6.14, "baby_cry"),
                (6.7, 9.01, "baby_cry"),
                (17.84, 18.02, "baby_cry"),
                (18.85, 19.33, "baby_cry"),
                (19.89, 20.45, "baby_cry"),
            ],
            [
                (11.56, 11.76, "baby_cry"),
                (12.31, 12.86, "baby_cry"),
                (13.98, 16.08, "baby_cry"),
                (16.89, 17.84, "baby_cry"),
                (21.66, 22.26, "baby_cry"),
            ],
            [
                (2.34, 3.55, "knocking"),
                (4.02, 4.13, "dog_bark"),
                (4.88, 5.01, "dog_bark"),
                (10.45, 12.02, "knocking"),
                (11.34, 11.46, "dog_bark"),
                (12.13, 12.29, "dog_bark"),
                (18.09, 18.17, "dog_bark"),
            ],
            [
                (0.0, 2.1, "baby_cry"),
                (0.2, 8.4, "aircon_noise"),
                (2.0, 4.2, "dog_bark"),
                (3.8, 6.0, "baby_cry"),
            ],
            [
                (2.11, 2.64, "baby_cry"),
                (3.92, 4.14, "baby_cry"),
                (5.62, 5.74, "baby_cry"),
                (6.31, 6.5, "baby_cry"),
                (6.5, 6.52, "baby_cry"),
                (7.5, 7.72, "baby_cry"),
            ],
        ]

    def test_merge_overlap(self):
        """Test that timelines with the same data label, which overlap are merged"""
        for (raw_data, expected) in zip(self.input_data, self.expected):
            self.assertListEqual(merge_overlaps(raw_data), expected)
