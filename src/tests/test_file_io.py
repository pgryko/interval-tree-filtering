import unittest
import os

from src.file_io import get_files, load_file_data


class TestGetFiles(unittest.TestCase):
    def test_get_files(self):
        """Given a directory returns a list of files with extension.lab"""
        directory = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "example_input_labels/"
        )
        # Expected behaviour - only load files with extension.lab (ignore not_a_labfile.txt)
        # Test that sequence first contains the same elements as second, regardless of their order.
        self.assertCountEqual(
            get_files(directory),
            [
                "example0.lab",
                "example1.lab",
                "malformed.lab",
                "example5.lab",
                "example3.lab",
                "example2.lab",
                "example6.lab",
                "example4.lab",
            ],
        )


class TestLoadFileData(unittest.TestCase):
    def setUp(self) -> None:
        self.directory = os.path.join(
            os.path.dirname(os.path.realpath(__file__)), "example_input_labels/"
        )

    def test_get_files_correct(self):
        """Test the loading of correctly formatted files of format extension.lab"""

        expected = {
            "example0.lab": [
                (0.0, 2.1, "baby_cry"),
                (0.2, 8.4, "aircon_noise"),
                (2.0, 3.4, "dog_bark"),
                (3.2, 4.2, "dog_bark"),
                (3.8, 6.0, "baby_cry"),
            ],
            "example1.lab": [
                (1.45, 2.06, "doorbell"),
                (3.51, 3.63, "dog_bark"),
                (4.23, 4.35, "dog_bark"),
                (4.99, 5.15, "dog_bark"),
                (6.49, 6.61, "dog_bark"),
                (7.7, 7.81, "dog_bark"),
                (11.4, 12.11, "dog_bark"),
                (11.59, 12.8, "dog_bark"),
                (16.3, 16.43, "dog_bark"),
                (26.86, 26.98, "dog_bark"),
            ],
            "example5.lab": [
                (5.45, 5.57, "baby_cry"),
                (5.74, 6.14, "baby_cry"),
                (6.7, 7.6, "baby_cry"),
                (7.53, 8.93, "baby_cry"),
                (8.67, 9.01, "baby_cry"),
                (17.84, 18.02, "baby_cry"),
                (18.85, 19.33, "baby_cry"),
                (19.89, 20.45, "baby_cry"),
            ],
            "example3.lab": [
                (11.56, 11.76, "baby_cry"),
                (12.31, 12.86, "baby_cry"),
                (13.98, 16.08, "baby_cry"),
                (16.89, 17.84, "baby_cry"),
                (12.31, 12.86, "baby_cry"),
                (21.66, 22.26, "baby_cry"),
            ],
            "example2.lab": [
                (2.34, 3.55, "knocking"),
                (4.02, 4.13, "dog_bark"),
                (4.88, 5.01, "dog_bark"),
                (10.45, 12.02, "knocking"),
                (11.34, 11.46, "dog_bark"),
                (12.13, 12.29, "dog_bark"),
                (18.09, 18.17, "dog_bark"),
            ],
            "example6.lab": [
                (0.0, 2.1, "baby_cry"),
                (0.2, 3.6, "aircon_noise"),
                (2.0, 3.4, "dog_bark"),
                (3.0, 4.0, "aircon_noise"),
                (3.2, 4.2, "dog_bark"),
                (3.5, 8.4, "aircon_noise"),
                (3.8, 6.0, "baby_cry"),
            ],
            "example4.lab": [
                (2.11, 2.64, "baby_cry"),
                (3.92, 4.14, "baby_cry"),
                (5.62, 5.74, "baby_cry"),
                (6.31, 6.5, "baby_cry"),
                (6.5, 6.52, "baby_cry"),
                (7.5, 7.72, "baby_cry"),
            ],
        }

        for file in expected.keys():
            self.assertListEqual(
                load_file_data(os.path.join(self.directory, file)), expected[file]
            )

    def test_get_files_error(self):
        """Test the loading of incorrectly formatted files of format extension.lab"""

        with self.assertRaises(Exception) as context:
            load_file_data(os.path.join(self.directory, "malformed.lab"))

        self.assertTrue(
            "could not convert string to float: 'baby_cry'" in str(context.exception)
        )
