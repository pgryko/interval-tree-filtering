from os.path import isfile, join
import os

import csv


def get_files(directory: str):
    """Given a directory returns a list of files with extension.lab"""
    only_files = []

    for f in os.listdir(directory):
        if isfile(join(directory, f)) and f.endswith(".lab"):
            only_files.append(f)
    return only_files


def load_file_data(filepath: str):
    data = []
    with open(filepath, newline="") as csvfile:
        data_reader = csv.reader(csvfile, delimiter=" ")

        for row in data_reader:
            data.append((float(row[0]), float(row[1]), str(row[2])))

    return data


def write_file_data(filepath: str, data):
    with open(filepath, "w", newline="") as csvfile:
        data_writer = csv.writer(csvfile, delimiter=" ")

        for row in data:
            # data.append((float(row[0]), float(row[1]), str(row[2])))
            data_writer.writerow(row)
